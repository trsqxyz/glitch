glitch
======

glitch jpg file

# Usage
`python -m glitch -i 'lena.jpg'`  
or  
`python -m glitch -i 'lena.jpg' -o 'glitched.jpg' -n 3`  
or  
`python -m glitch -i 'lena.jpg' max`  

## ENVIRONMENT  
2014-06-19 22:04:33.609234  
Python 3.4.0  
### OS  
ProductName:	Mac OS X  
ProductVersion:	10.9.3  
BuildVersion:	13D65
### REQUIREMENTS
- docopt: 0.6.1  
